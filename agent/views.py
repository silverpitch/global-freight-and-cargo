from django.shortcuts import render
from track.forms import new_track,edittrack,new_security,editsecurity
from track.models import track,security_model
from django.http import HttpResponse
from home.models import UserExtended
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponse

# Create your views here.

@login_required(login_url='/accounts/signin/')
def index(request):
	user = request.user
	forms = track.objects.all()
	form = new_track()
	return render(request, 'agent/index.html',{'user':user,'form':form,'forms':forms})



@login_required(login_url='/accounts/login/')
def new(request):
	if request.method == 'POST':
		form = new_track(request.POST)
		if form.is_valid():
			reference_number = request.POST['reference_number']
			goods_from = request.POST['goods_from']
			goods_to = request.POST['goods_to']
			depature_date = request.POST['depature_date']
			arrival_date = request.POST['arrival_date']
			goods_description = request.POST['goods_description']
			current_status = request.POST['current_status']
			sender = request.POST['sender']
			reciever = request.POST['reciever']
			sender_address = request.POST['sender_address']
			reciever_address = request.POST['reciever_address']
			depature_time = request.POST['depature_time']
			arrival_time = request.POST['arrival_time']			
			save_track = track.objects.create(reference_number=reference_number,
												goods_from=goods_from,goods_to=goods_to,
												depature_date=depature_date,
												arrival_date=arrival_date,
												goods_description=goods_description,
												current_status=current_status,
												sender=sender,
												reciever=reciever,
												sender_address=sender_address,
												reciever_address=reciever_address,
												depature_time=depature_time,
												arrival_time=arrival_time)
			message = 'saved'
			return HttpResponse('saved')
	else:
		return HttpResponseRedirect('/agent/')



def update(request):
	ob = security_model.objects.get(track_number="GFC1504292614")
	ob.delete()
	return HttpResponse("Updated")




@login_required(login_url='/accounts/signin/')
def delete(request,track_form_id):
	form = track.objects.get(id=track_form_id).delete()
	return HttpResponseRedirect('/agent/')



@login_required(login_url='/accounts/signin/')
def edit(request,track_form_id):
	if request.method=='POST':
		#do nothing
		model2= track.objects.get(id=track_form_id)
		track_edit = edittrack(request.POST,instance=model2)
		track_edit.save()
		return HttpResponseRedirect('/agent/')
	else:
		model= track.objects.get(id=track_form_id)
		form = edittrack(instance=model)
		return render(request,'agent/edit.html',{'form':form})


@login_required(login_url='/accounts/signin/')
def security(request):
	user = request.user
	forms = security_model.objects.all()
	form = new_security()
	return render(request, 'agent/security.html',{'user':user,'form':form,'forms':forms})


@login_required(login_url='/accounts/signin/')
def newsecurity(request):
	if request.method == 'POST':
		form = new_security(request.POST)
		depositor_name = request.POST['depositor_name']
		depositer_address = request.POST['depositer_address']
		reciever_officer_name  = request.POST['reciever_officer_name']
		date_of_deposit = request.POST['date_of_deposit']
		type_of_consignment = request.POST['type_of_consignment']
		origin = request.POST['origin']
		track_number = request.POST['track_number']
		goods_description = request.POST['goods_description']
		status = request.POST['status']
		save_security = security_model.objects.create(depositor_name=depositor_name,
													depositer_address=depositer_address,
													reciever_officer_name=reciever_officer_name,
													date_of_deposit=date_of_deposit,
													type_of_consignment=type_of_consignment,
													origin=origin,
													track_number=track_number,
													goods_description=goods_description,
													status=status)
		message = 'security form saved'
		return HttpResponseRedirect('/agent/')
	else:
		return HttpResponseRedirect('/agent/')




@login_required(login_url='/accounts/signin/')
def delete_security(request,track_form_id):
	form = security_model.objects.get(id=track_form_id).delete()
	return HttpResponseRedirect('/agent/security/')



@login_required(login_url='/accounts/signin/')
def edit_security(request,track_form_id):
	if request.method=='POST':
		#do nothing
		model2= security_model.objects.get(id=track_form_id)
		track_edit = editsecurity(request.POST,instance=model2)
		track_edit.save()
		return HttpResponseRedirect('/agent/security/')
	else:
		model= security_model.objects.get(id=track_form_id)
		form = editsecurity(instance=model)
		return render(request,'agent/edit.html',{'form':form})