from django.conf.urls import patterns, include, url

from agent import views

urlpatterns = patterns('',
    # Examples:
	url(r'^$', 'agent.views.index'),
	url(r'^new/', 'agent.views.new'),
	url(r'^newsecurity/', 'agent.views.newsecurity'),
    url(r'^update/', 'agent.views.update'),
    url(r'^security/', 'agent.views.security'),
    url(r'^delete/(?P<track_form_id>\d+)/', views.delete, name="delete"),
    url(r'^edit/(?P<track_form_id>\d+)/', views.edit, name="edit"),
    url(r'^delete_security/(?P<track_form_id>\d+)/', views.delete_security, name="delete_security"),
    url(r'^edit_security/(?P<track_form_id>\d+)/', views.edit_security, name="edit_security"),
)
