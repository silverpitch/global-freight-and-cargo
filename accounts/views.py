from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.contrib.auth import authenticate, login , logout
from django.contrib.auth.models import User
from django.contrib.sessions.backends.db import SessionStore
from home.models import UserExtended
from accounts.forms import signinform, signupform




def signin(request):
	if request.method == 'POST': # if the form on the sign in page was actually submitted
		#it was submitted hence collect data
		username = request.POST['Username']
		password = request.POST['Password']
		form = signinform(request.POST)
		if form.is_valid():
			try:
				user = User.objects.get(username=username)
				user = authenticate(username=username, password=password)
				if user is not None:
					if user.is_active:
						login(request, user) # authentication past user is free to login
						return HttpResponseRedirect('/agent/')
					else:
						return HttpResponse("Account is not active")
				else:
					message = 'The Username or Password was wrong. Please try again.'
					return render(request, 'accounts/signin/index.html',{'message':message,'form':form})
				
			except User.DoesNotExist:
				#the user was not found in the datatbase
				form = signinform()
				message = 'No account exists in such a name.'
				return render(request, 'accounts/signin/index.html',{'message':message,'form':form})	
		else:
			form = signinform()
			message = 'The form was not filled out correctly. Please fill in again.'
			return render(request, 'accounts/signin/index.html',{'message':message,'form':form})	
	else:
		# the form was not submitted hence render sign in page
		form = signinform()
		return render(request, 'accounts/signin/index.html',{'form':form})




def signup(request):
	users = User.objects.all().count()
	if users >= 1:
		return HttpResponseRedirect('/accounts/signin/')
	else:
		if request.method == 'POST': # if the form on the sign in page was actually submitted
			#it was submitted hence collect data
			username = request.POST['Username']
			password = request.POST['Password']
			email = request.POST['Email']
			first_name = request.POST['first_name']
			second_name = request.POST['second_name']
			form = signupform(request.POST)
			if form.is_valid():
				try:
					u = User.objects.get(username__exact=username)
				except User.DoesNotExist:
					user = User.objects.create_user(username, email, password)
					user.first_name = first_name
					user.second_name = second_name
					user.save()
					user = User.objects.get(username = username)
					if user.id == 1 :
						role='admin'
					else:
						role='normal'
					extended = UserExtended.objects.create(user_id=user.id,role=role)
					user = authenticate(username=username, password=password)
					login(request,user)
					return HttpResponseRedirect('/agent/')
				message = 'The specified user already exists.'
				return render(request, 'accounts/signup/index.html',{'message':message,'form':form})
			else:
				form = signupform()
				message = 'The submitted form was invalid, please fill it again.'
				return render(request, 'accounts/signup/index.html',{'message':message,'form':form})

		else:
			form = signupform()
			return render(request, 'accounts/signup/index.html',{'form':form})

		
def signout_view(request):
    logout(request)
    return HttpResponseRedirect('/home/')
