from django.conf.urls import patterns, include, url



urlpatterns = patterns('',
    # Examples:
	url(r'^$', 'track.views.index'),
	url(r'^view/', 'track.views.view'),
	url(r'^security/', 'track.views.security_index'),
	url(r'^track_security/', 'track.views.security_view'),
)
