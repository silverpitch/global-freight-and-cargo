from django.shortcuts import render
from models import track,security_model
from .forms import track_request_form

def index(request):
	form = track_request_form()
	return render(request, 'track/index.html',{'form':form})

def view(request):
	form = track_request_form()
	track_id = request.POST['track_id']
	try:
		track_reponse = track.objects.get(reference_number=track_id)
		return render(request, 'track/index.html',{'form':form,'track_response':track_reponse})
	except Exception as e:
		error = e
		return render(request, 'track/index.html',{'form':form,'error':error})


def security_index(request):
	form = track_request_form()
	return render(request, 'track/security.html',{'form':form})



def security_view(request):
	form = track_request_form()
	track_id = request.POST['track_id']
	try:
		track_reponse = security_model.objects.get(track_number=track_id)
		return render(request, 'track/security.html',{'form':form,'track_response':track_reponse})
	except Exception as e:
		error = e
		return render(request, 'track/security.html',{'form':form,'error':error})