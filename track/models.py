from django.db import models
from django.forms import ModelForm

class track(models.Model):
	sender = models.CharField(max_length=1055)
	sender_address = models.CharField(max_length=1055)
	reciever_address = models.CharField(max_length=1055)
	reciever = models.CharField(max_length=1005)
	reference_number = models.CharField(max_length=1005)
	goods_from = models.CharField(max_length=1055)
	goods_to = models.CharField(max_length=1055)
	depature_date = models.CharField(max_length=1055)
	depature_time = models.CharField(max_length=1055)
	arrival_date = models.CharField(max_length=1055)
	arrival_time = models.CharField(max_length=1055)
	goods_description = models.CharField(max_length=1550)
	current_status = models.CharField(max_length=1055)


class security_model(models.Model):
	depositor_name = models.CharField(max_length=1055)
	depositer_address = models.CharField(max_length=1055)
	reciever_officer_name = models.CharField(max_length=1055)
	date_of_deposit = models.CharField(max_length=1055)
	type_of_consignment = models.CharField(max_length=1055)
	origin = models.CharField(max_length=1055)
	track_number = models.CharField(max_length=1055)
	goods_description = models.CharField(max_length=1355)
	status = models.CharField(max_length=1055)