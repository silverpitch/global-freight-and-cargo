from django import forms
from django.forms import ModelForm,Textarea
from track.models import track,security_model

class track_request_form(forms.Form):
	track_id = forms.CharField(label='Enter your track ID', max_length=100, widget=forms.TextInput(attrs={'class':'form-control'}))

class new_track(forms.Form):
	sender = forms.CharField(label='Sender Name',max_length=1055, widget=forms.TextInput(attrs={'class':'form-control'}))
	sender_address = forms.CharField(label='Sender Address',max_length=1055, widget=forms.TextInput(attrs={'class':'form-control'}))
	reciever = forms.CharField(label='Reciever Name',max_length=1055, widget=forms.TextInput(attrs={'class':'form-control'}))
	reciever_address = forms.CharField(label='Reciever Address',max_length=1055, widget=forms.TextInput(attrs={'class':'form-control'}))
	reference_number = forms.CharField(label='reference ID',max_length=1055, widget=forms.TextInput(attrs={'class':'form-control'}))
	goods_from = forms.CharField(label='Goods are From',max_length=1055, widget=forms.TextInput(attrs={'class':'form-control'}))
	goods_to = forms.CharField(label='Goods To',max_length=1055, widget=forms.TextInput(attrs={'class':'form-control'}))
	depature_date = forms.CharField(label='Depature Date',max_length=1055, widget=forms.TextInput(attrs={'class':'form-control'}))
	depature_time = forms.CharField(label='Depature time',max_length=1055, widget=forms.TextInput(attrs={'class':'form-control'}))
	arrival_date = forms.CharField(label='Arrival Date',max_length=1055, widget=forms.TextInput(attrs={'class':'form-control'}))
	arrival_time= forms.CharField(label='Arrival Time',max_length=1055, widget=forms.TextInput(attrs={'class':'form-control'}))
	goods_description = forms.CharField(label='Goods Description',max_length=1060, widget=forms.Textarea(attrs={'class':'form-control'}))
	current_status = forms.CharField(label='Status',max_length=1055, widget=forms.Textarea(attrs={'class':'form-control'}))



class edittrack(ModelForm):
	class Meta:
		model = track
		fields = ['sender','sender_address','reciever_address','reciever','reference_number','goods_from','goods_to','depature_date','depature_time','arrival_date','arrival_time','goods_description','current_status']
		widgets = {
		            'goods_description': Textarea(attrs={'class':'form-control'}),
					'sender': Textarea(attrs={'class':'form-control'}),
					'sender_address': Textarea(attrs={'class':'form-control'}),
					'reciever_address': Textarea(attrs={'class':'form-control'}),
					'reciever': Textarea(attrs={'class':'form-control'}),
					'reference_number': Textarea(attrs={'class':'form-control'}),
					'goods_from': Textarea(attrs={'class':'form-control'}),
					'goods_to': Textarea(attrs={'class':'form-control'}),
					'depature_date': Textarea(attrs={'class':'form-control'}),
					'depature_time': Textarea(attrs={'class':'form-control'}),
					'arrival_time': Textarea(attrs={'class':'form-control'}),
					'arrival_date': Textarea(attrs={'class':'form-control'}),
					'current_status': Textarea(attrs={'class':'form-control'}),
		        }
				

class new_security(forms.Form):
	depositor_name = forms.CharField(label='depositor name',max_length=1055, widget=forms.TextInput(attrs={'class':'form-control'}))
	depositer_address = forms.CharField(label='depositer address',max_length=1055, widget=forms.TextInput(attrs={'class':'form-control'}))
	reciever_officer_name = forms.CharField(label='reciever officer name',max_length=1055, widget=forms.TextInput(attrs={'class':'form-control'}))
	date_of_deposit = forms.CharField(label='Date of Deposit',max_length=1055, widget=forms.TextInput(attrs={'class':'form-control'}))
	type_of_consignment = forms.CharField(label='type of consignment',max_length=1055, widget=forms.TextInput(attrs={'class':'form-control'}))
	origin = forms.CharField(label='origin',max_length=1055, widget=forms.TextInput(attrs={'class':'form-control'}))
	track_number = forms.CharField(label='track number',max_length=55, widget=forms.TextInput(attrs={'class':'form-control'}))
	goods_description  = forms.CharField(label='Goods Description',max_length=1360, widget=forms.Textarea(attrs={'class':'form-control'}))
	status = forms.CharField(label='status',max_length=1055, widget=forms.Textarea(attrs={'class':'form-control'}))



class editsecurity(ModelForm):
	class Meta:
		model = security_model
		fields = ['depositor_name','depositer_address','reciever_officer_name','date_of_deposit','type_of_consignment','origin','track_number','goods_description','status']
		widgets = {
		            'goods_description': Textarea(attrs={'class':'form-control'}),
		            'depositor_name': Textarea(attrs={'class':'form-control'}),
		            'depositer_address': Textarea(attrs={'class':'form-control'}),
		            'reciever_officer_name': Textarea(attrs={'class':'form-control'}),
					'current_status': Textarea(attrs={'class':'form-control'}),
					'depositer_address': Textarea(attrs={'class':'form-control'}),
		        }		