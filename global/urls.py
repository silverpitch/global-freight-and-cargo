from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = patterns('',
    # Examples:
    url(r'^$', include('home.urls')),
     url(r'^accounts/', include('accounts.urls',namespace="accounts")),
     url(r'^agent/', include('agent.urls',namespace="agent")),
     url(r'^track/', include('track.urls',namespace="track")),
url(r'^home/', include('home.urls')),

)+static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
