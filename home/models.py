from django.db import models
from django.forms import ModelForm
from django.contrib.auth.models import User
    


# Create your models here.
class UserExtended(models.Model):
        user = models.OneToOneField(User)
        role = models.CharField(max_length=15)

        def __unicode__(self):
                return self.brief_description, self.role



