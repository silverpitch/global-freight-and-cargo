from django.conf.urls import patterns, include, url

from home import views
urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'profilespoint.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),url(r'^admin/', include(admin.site.urls)),

    url(r'^$', 'home.views.index', name='index'),

)
